## shell 轮子
>**目前仅适用于CentOS/RHEL 6+**

### 安装
- 一键安装

```bash
yum -y install git screen #不同系统安装有差异，请参考相关系统命令
screen -S lnmp #避免长时间安装，终端关闭导致安装失败
git  clone https://git.oschina.net/careyjike_173/script.git &&  cd script; ./install.sh | tee -a install.log
```

- 安装完成后执行

```bash
source /etc/profile
```

- php模块添加

```bash
chmod +x tools/add_mode_php.sh; ./tools/add_mode_php.sh
```

### 安装清单
- Nginx
- Tengine
- Apache (apache 2.2.34 为最终维护版，[详情查看](http://mail-archives.apache.org/mod_mbox/www-announce/201707.mbox/%3CCACsi2512a0dKZm5SEb9GyNH6nMfs1+swpxyui3c+UZUwvi3vvg@mail.gmail.com%3E))
- Tomcat
- MySQL
- PHP
- ImageMagick
- GraphicsMagick
- ZendOpcache
- XCache
- APCU
- Jemalloc
- PureFtpd
- Redis
- Memcache
- Ssdb
- Gitlab-ce

### 相关目录
- conf
相关配置文件
- include
服务安装脚本
- init.d
部分启动脚本
- public
公共配置，如：`版本号`，`安装路径`,`下载`等
- src
安装包存放目录
- tools
其它工具

### 注意
>**尽量使用全新的环境**

>如果需要修改版本号和安装目录等，请修改 `public/versions.conf` 和 `public/options.conf` 文件。
>国内请参考**[mirrors](https://carey.akhack.com/mirror/index.html)**修改版本号，如发现你想要的版本号不存在，请手动下载到`src`目录下即可或通过问题反馈
>`master`为稳定版本，`dev`为开发版本。

### 问题反馈
欢迎通过[Issues](http://git.oschina.net/careyjike_173/script/issues)反馈，或者发送email:[carey@akhack.com]()

### blog
[https://carey.akhack.com](https://carey.akhack.com)


