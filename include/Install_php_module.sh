#!/bin/bash


Install_Php_Module() {
	local PHP_VERSION=`${php_install_dir}/bin/php-config --version`
  local PHP_INI_DIR=`${php_install_dir}/bin/php --ini | awk NR==2 | awk '{print $NF}'`
  local PHP_EXTSION_DIR=`${php_install_dir}/bin/php-config --extension-dir`

  pushd ${Pwd}/src
  if [ -d "php-${PHP_VERSION}" ]; then
  	pushd php-${PHP_VERSION}/ext/${module_name}
  elif [ -f "php-${PHP_VERSION}.tar.gz" ]; then
  	tar zxf php-${PHP_VERSION}.tar.gz
  	pushd php-${PHP_VERSION}/ext/${module_name}
  else
  	src_url=http://www.php.net/distributions/php-${PHP_VERSION}.tar.gz && wget -c --tries=6 $src_url
  	tar zxf php-${PHP_VERSION}.tar.gz
  	pushd php-${PHP_VERSION}/ext/${module_name}
  fi

  ${php_install_dir}/bin/phpize
  ./configure --with-php-config=${php_install_dir}/bin/php-config
  make -j ${THREAD} && make install
  if [ -f "${PHP_EXTSION_DIR}/${module_name}.so" ]; then
  		grep "extension=${PHP_EXTSION_DIR}/${module_name}.so" ${PHP_INI_DIR} || echo "extension=${PHP_EXTSION_DIR}/${module_name}.so" >> ${PHP_INI_DIR}
  		/etc/init.d/php-fpm restart
  		${php_install_dir}/bin/php -m |grep ${module_name} && echo -e "${CSUCCESSFUL}php extension ${module_name}  install successfull ${CEND}"
  else
  	echo -e "${CFAIL} php extension ${module_name} install filed ${CEND}"
  	kill -9 $$
  fi
}