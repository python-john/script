#!/bin/bash

Install_Gitlab-ce() {

  if [ "$Countries" = 'CN' ]; then
	  mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.backup
	  wget -O /etc/yum.repos.d/CentOS-Base.repo http://mirrors.aliyun.com/repo/Centos-7.repo
	fi

  systemctl stop firewalld.service
  yum -y install policycoreutils policycoreutils-python openssh-server openssh-clients postfix git
  systemctl start postfix

  # install gitlab-ce
  curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash
  yum -y install gitlab-ce

  # configure gitlab
  gitlab-ctl reconfigure

  if [ $? -eq 0 ]; then
  	echo -e "${CSUCCESSFUL} Gitlab-ce install successfull${CEND}"
  else
  	echo -e "${CFAIL}Error, gitlab-ce install faild ${CEND}"
  	kill -9 $$
  fi
}
