#!/bin/bash

Install_Jenkins() {

	# wget repo
  yum -y install java-1.8.0
  wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins.io/redhat/jenkins.repo
  rpm --import http://pkg.jenkins.io/redhat/jenkins.io.key
	ln -s /usr/java/jdk1.8.0_121/bin/java /usr/bin/java
  # install jenkins
  yum install -y jenkins
  if [ $? -ne 0 ]; then
  	echo -e "${CSUCCESSFUL} Gitlab-ce install successfull${CEND}"
  else
  	echo -e "${CFAIL}Error, jenkins install faild ${CEND}"
  	kill -9 $$
  fi

  systemctl start jenkins
}
