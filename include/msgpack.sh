#!/bin/bash

Install_Msgpack() {
	pushd ${Pwd}/src

	phpExtensionDir=$(${php_install_dir}/bin/php-config --extension-dir)

	git clone https://github.com/msgpack/msgpack-php.git
  pushd msgpack-php
  ${php_install_dir}/bin/phpize
  ./configure --with-php-config=${php_install_dir}/bin/php-config
  make && make install
  popd

  if [ -f "${phpExtensionDir}/msgpack.so" ]; then
  	echo "extension=msgpack.so" > ${php_install_dir}/etc/php.d/ext-msgpack.ini
  fi

  ${php_install_dir}/bin/php -m | grep msgpack
  if [ $? -eq 0 ]; then
	  echo -e "${CSUCCESSFUL}PHP msgpack module installed successfully! ${CEND}"
	else
	  echo -e "${CFAIL}PHP msgpack module install failed, Please contact the author! ${CEND}"
	fi

}