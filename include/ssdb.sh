#!/bin/bash
Install_Ssdb() {
	pushd ${Pwd}/src
  if [ -f "${Ssdb_Version}.tar.gz" ]; then
    tar zxf ${Ssdb_Version}.tar.gz && cd ssdb-${Ssdb_Version}
    make && make install
    if [ -f "/usr/local/ssdb/ssdb.conf" ]; then
    	echo -e "${CSUCCESSFUL} ssdb-${Ssdb_Version} install successful ${CEND}"
  	  cp tools/ssdb.sh /etc/init.d/ssdb
  	  chmod +x /etc/init.d/ssdb
  	  sed -i 's/configs=.*/configs=${ssdb_root}\/ssdb.conf/g' /etc/init.d/ssdb
  	  sed -i 's/ip: 127.0.0.1/ip: 0.0.0.0/g' /usr/local/ssdb/ssdb.conf
  	  sed -i 's/#auth:.*/auth: 4fa3337c1b2a5831d77e808e5b51f02a/g' /usr/local/ssdb/ssdb.conf
  	  /etc/init.d/ssdb start
    else
    	echo -e "${CFAIL}Error, ssdb-${Ssdb_Version} install faild ${CEND}"
    fi
  fi
  popd
}