Install_Ssh2() {
	pushd ${Pwd}/src

	if [ -e "${php_install_dir}/bin/phpize" ]
		phpExtensionDir=`${php_install_dir}/bin/php-config --extension-dir`
		
		if [ ! -f "libssh2-${libssh2_version}.tar.gz" ]; then
			src_url=https://libssh2.org/download/libssh2-${libssh2_version}.tar.gz && wget -c --tries=6 $src_url
		fi
		
		if [ "`${php_install_dir}/bin/php -r 'echo PHP_VERSION;' | awk -F. '{print $1}'`" == '7' ]; then
			if [ ! -f "ssh2-${ssh2_version}.tgz" ]; then
				src_url=http://pecl.php.net/get/ssh2-${ssh2_version}.tgz && wget -c --tries=6 $src_url
			fi
		else
			if [ ! -f "ssh2-${ssh2_php7_version}.tgz" ]; then
				src_url=http://pecl.php.net/get/ssh2-${ssh2_php7_version}.tgz && wget -c --tries=6 $src_url
			fi
		fi

		tar zxf libssh2-${libssh2_version}.tar.gz && pushd libssh2-${libssh2_version}
		./configure && make -j ${THREAD} && make install
		popd
		
		tar zxf ssh2-${ssh2_version}.tgz && pushd ssh2-${ssh2_version}
		${php_install_dir}/bin/phpize
		./configure --with-php-config=${php_install_dir}/bin/php-config --with-ssh2 && make -j ${THREAD} && make install
		if [ -f "${phpExtensionDir}/ssh2.so" ]; then
		  echo 'extension=ssh2.so' > ${php_install_dir}/etc/php.d/ext-ssh2.ini
		  echo -e "${CSUCCESSFUL}PHP Redis module installed successfully! ${CEND}"
		  popd
		else
		  echo -e "${CFAIL}PHP Redis module install failed, Please contact the author! ${CEND}"
		fi
	fi


}
