#!/bin/bash

Install_Tmux() {
  pushd ${Pwd}/src

  if [ ! -e "$(which tmux)" ]; then
    # Install libevent first
    tar xzf libevent-${libevent_version}.tar.gz
    pushd libevent-${libevent_version}
    ./configure
    make -j ${THREAD} && make install
    popd
    rm -rf libevent-${libevent_version}

    # install tmux
    tar xzf tmux-${tmux_version}.tar.gz
    pushd tmux-${tmux_version}
    CFLAGS="-I/usr/local/include" LDFLAGS="-L//usr/local/lib" ./configure
    make -j ${THREAD} && make install
    unset LDFLAGS
    popd

    if [ "${OS_BIT}" == "64" ]; then
      ln -s /usr/local/lib/libevent-2.0.so.5 /usr/lib64/libevent-2.0.so.5
    else
      ln -s /usr/local/lib/libevent-2.0.so.5 /usr/lib/libevent-2.0.so.5
    fi
  fi
}

Install_Htop() {
  # install htop

  if [ ! -e "$(which htop)" ]; then
    tar xzf htop-${htop_version}.tar.gz
    pushd htop-${htop_version}
    ./configure
    make -j ${THREAD} && make install
    popd
  fi
  popd
}
