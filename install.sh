#!/bin/bash
#
# Author:  carey <carey@akhack.com>
# BLOG:  https://carey.akhack.com
#
# Apply to: CentOS/REHL 6+
#

# set work directory
sed -i "s@^Pwd.*@Pwd=`pwd`@" ./public/options.conf

# Load public config
. ./public/preload.sh

. ./include/init_centos.sh

while :; do echo
  read -p "Do you want to Init System [y/n]" Init_yn
  if [[ $Init_yn =~ ^[y,n]$ ]]; then
    while :; do echo
      read -p "Do you want to install Tmux and htop [y/n]" Tmux_Htop_yn
      if [[ $Tmux_Htop_yn =~ ^[y,n]$ ]]; then
        break
      else
        echo -e "${CFAIL} Error,Only input 'y' or 'n'... ${CEND} "
      fi
    done
    break
  else
    echo -e "${CFAIL} Error,Only input 'y' or 'n'... ${CEND} "
  fi
done

# web server
while :; do echo
  read -p "Do you want to install web server [y/n]?" Web_yn
  if [[  $Web_yn =~ ^[y,n]$ ]]; then
    if [ "${Web_yn}" == 'y' ]; then
      select Web_var in "Install Nginx" "Install Apache" "Install Tengine" "Install Tomcat"; do
        if [ "${Web_var}" = "Install Nginx" ]; then
          break
        elif [ "${Web_var}" = "Install Apache" ]; then
          select Apache_var in "Apache-2.2" "Apache-2.4"; do
              break
          done
        elif [ "${Web_var}" = "Install Tomcat" ]; then
          select Tomcat_var in "Tomcat-7" "Tomcat-8"; do
            select Jdk_var in "Jdk-1.7" "Jdk-1.8"; do
              break
            done
            break
          done
        fi
        break
      done
      break
    elif [ "${Web_yn}" == 'n' ]; then
        break
    fi
    break
  else
    echo -e "${CFAIL} Error,Only input 'y' or 'n'... ${CEND} "
  fi
done

# database server
while :; do echo
  read -p "Do you want to install database server [y/n]?" Db_yn
  if [[  $Db_yn =~ ^[y,n]$ ]]; then
    if [ "${Db_yn}" == 'y' ]; then
      select Db_var in "Install Mysql" "Install Mariadb"; do
        if [ "${Db_var}" = "Install Mysql" ]; then
          select Mysql_var in "Mysql-5.5" "Mysql-5.6" "Mysql-5.7"; do
            while :; do
              read -p "Please Enter the database install model [source | binary] :" install_mod
              if [ -n "${install_mod}" ]; then
                sed -i "s@^install_mod.*@install_mod=${install_mod}@" ./public/options.conf
                break
              fi
            done
            while :; do
              read -p "Please Enter the password for the database： " dbrootpwd
              if [ -n "${dbrootpwd}" ]; then
                sed -i "s@^dbrootpwd.*@dbrootpwd=${dbrootpwd}@" ./public/options.conf
                break
              fi
            done
            break
          done
        elif [ "${Db_var}" = "Install Mariadb" ]; then
          select Mysql_var in "Mariadb-10.0" "Mariadb-10.1" "Mariadb-5.5"; do
            while :; do
              read -p "Please Enter the database install model[source | binary] :" install_mod
              if [ -n "${install_mod}" ]; then
                sed -i "s@^install_mod.*@install_mod=${install_mod}@" ./public/options.conf
                break
              fi
            done
            while :; do
              read -p "Please Enter the password for the database： " dbrootpwd
              if [ -n "${dbrootpwd}" ]; then
                sed -i "s@^dbrootpwd.*@dbrootpwd=${dbrootpwd}@" ./public/options.conf
                break
              fi
            done
            break
          done
        fi
        break
      done
      break
    elif [ "${Web_yn}" == 'n' ]; then
      break
    fi
    break
  else
    echo -e "${CFAIL} Error,Only input 'y' or 'n'... ${CEND} "
  fi
done

if [[ "${Db_yn}" =~ 'n' ]]; then
  unset dbrootpwd
fi

# php server
while :; do echo
  read -p "Do you want to install php server [y/n]?" Php_yn
  if [[  $Php_yn =~ ^[y,n]$ ]]; then
    if [ "${Php_yn}" == 'y' ]; then
      select Php_var in "Php-5.3" "Php-5.4" "Php-5.5" "Php-5.6" "Php-7.0" "Php-7.1"; do
        if [ "${Php_var}" = "Php-5.3" ]; then
          select Php_cache in "Zend OPcache" "XCache" "APCU"; do break;done
        elif [ "${Php_var}" = "Php-5.4" ]; then
          select Php_cache in "Zend OPcache" "XCache" "APCU"; do break;done
        elif [ "${Php_var}" = "Php-5.5" ]; then
          select  Php_cache in "Zend OPcache" "XCache" "APCU"; do break;done
        elif [ "${Php_var}" = "Php-5.6" ]; then
          select Php_cache in "Zend OPcache" "XCache" "APCU"; do break;done
        elif [ "${Php_var}" = "Php-7.0" ] ||  [ "${Php_var}" = "Php-7.1" ]; then
          select Php_cache in "Zend OPcache" "APCU"; do break;done
        fi
        break
      done
      break
    elif [ "${Web_yn}" == 'n' ]; then
      break
    fi
    break
  else
    echo -e "${CFAIL} Error,Only input 'y' or 'n'... ${CEND} "
  fi
done

# ImageMagick or GraphicsMagick
while :; do echo
  read -p "Do you want to install ImageMagick or GraphicsMagick [y/n]?" Magick_yn
  if [[  $Magick_yn =~ ^[y,n]$ ]]; then
    if [ "${Magick_yn}" == 'y' ]; then
      select Magick_var in "ImageMagick" "GraphicsMagick"; do
        break
      done
      break
    elif [ "${Magick_yn}" == 'n' ]; then
      break
    fi
    break
  else
    echo -e "${CFAIL} Error,Only input 'y' or 'n'... ${CEND} "
  fi
done

# pureftpd server
while :; do echo
  read -p "Do you want to install pureftpd [y/n]?" Ftp_yn
  if [[  $Ftp_yn =~ ^[y,n]$ ]]; then
    break
  else
    echo -e "${CFAIL} Error,Only input 'y' or 'n'... ${CEND} "
  fi
done

# redis server
while :; do echo
  read -p "Do you want to install redis [y/n]?" Redis_yn
  if [[ $Redis_yn =~ ^[y,n]$ ]]; then
    break
  else
    echo -e "${CFAIL}Error,Only input 'y' or 'n'...${CEND}"
  fi
  break
done

# memcached server
while :; do echo
  read -p "Do you want to install memcached [y/n]?" Memcached_yn
  if [[ $Memcached_yn =~ ^[y,n]$ ]]; then
    if [ "${Memcached_yn}" = 'y' ]; then
      if [ ${Php_var} ]; then
        select Mem_var in "php-extension-memcache" "php-extension-memcached"; do break; done
      fi
    fi
    break
  else
    echo -e "${CFAIL}Error,Only input 'y' or 'n'...${CEND}"
  fi
  break
done

# ssdb server
while :; do echo
  read -p "Do you want to install ssdb [y/n]?" Ssdb_yn
  if [[  "${Ssdb_yn}" =~ ^[y,n]$ ]]; then
    break
  else
    echo -e "${CFAIL}Error,Only input 'y' or 'n'... ${CEND}"
  fi
done

# gitlab-ce
while :; do echo
  read -p "Do you want to install gitlab-ce [y/n]?" Gitlab_yn
  if [[ "${Gitlab_yn}" =~ ^[y,n]$ ]]; then
    break
  else
    echo -e "${CFAIL}Error,Only input 'y' or 'n'... ${CEND}"
  fi
done

# jenkins
# while :; do echo
#   read -p "Do you want to install jenkins [y/n]?" Jenkins_yn
#   if [[ "${Jenkins_yn}" =~ ^[y,n]$ ]]; then
#     break
#   else
#     echo -e "${CFAIL}Error,Only input 'y' or 'n'... ${CEND}"
#   fi
# done


############

printf '''
==================================================
======  Is about to start the installation  ======
======  NOT INSTALL, Please Ctrl + C ...    ======
==================================================
'''

sleep 3

# download
yum -y install wget
Download


# Init
if [ "${Init_yn}" = "y" ]; then
  Init_Centos
fi

# tmux htop
if [ "${Tmux_Htop_yn}" = "y" ]; then
  . ./include/tmux-htop.sh
  Install_Tmux
  Install_Htop
fi

# jemalloc
. ./include/jemalloc.sh
Install_Jemalloc

array=()
# database
if [ "${Mysql_var}" = "Mysql-5.5" ]; then
  . ./include/MySQL5.5.sh
  Install_MySQL55 && array[0]=MySQL-${mysql55_version}
elif [ "${Mysql_var}" = "Mysql-5.6" ]; then
  . ./include/MySQL5.6.sh
  Install_MySQL56 && array[1]=MySQL-${mysql56_version}
elif [ "${Mysql_var}" = "Mysql-5.7" ]; then
  . ./include/Boost.sh
  . ./include/MySQL5.7.sh
  Install_Boost
  Install_MySQL57 && array[2]=MySQL-${mysql57_version}
elif [ "${Mysql_var}" = "Mariadb-10.0" ]; then
  . ./include/MariaDB10.0.sh
  Install_MariaDB100 && array[3]=MariaDB-${mariadb100_version}
elif [ "${Mysql_var}" = "Mariadb-10.1" ]; then
  . ./include/MariaDB10.1.sh
  Install_MariaDB101 && array[4]=Mariadb-${mariadb101_version}
elif [ "${Mysql_var}" = "Mariadb-5.5" ]; then
  . ./include/MariaDB5.5.sh
  Install_MariaDB55 && array[6]=Mariadb-${mariadb55_version}
fi

# web server
if [ "${Web_var}" = "Install Nginx" ]; then
  . ./include/Nginx.sh
  Install_Nginx && array[7]=Nginx-${nginx_version}
elif [ "${Web_var}" = "Install Tengine" ]; then
  . ./include/Tengine.sh
  Install_Tengine && array[8]=Tengine-${tengine_version}
elif [ "${Apache_var}" = "Apache-2.2" ]; then
  . ./include/Apache2.2.sh
  Install_Apache22 && array[9]=Apache-${apache22_version}
elif [ "${Apache_var}" = "Apache-2.4" ]; then
  . ./include/Apache2.4.sh
  Install_Apache24 && array[10]=Apache-${apache24_version}
elif [ "${Tomcat_var}" = "Tomcat-7" ]; then
  if [ "${Jdk_var}" = "Jdk-1.7" ]; then
      . ./include/Jdk1.7.sh
      Install-JDK17
  elif [ "${Jdk_var}" = "Jdk-1.8" ]; then
      . ./include/Jdk-1.8.sh
      Install-JDK18
  fi
  . ./include/Tomcat7.sh
  Install_Tomcat7 && array[11]=Tomcat-${tomcat7_version}
elif [ "${Tomcat_var}" = "Tomcat-8" ]; then
  if [ "${Jdk_var}" = "Jdk-1.7" ]; then
      . ./include/Jdk1.7.sh
      Install-JDK17
  elif [ "${Jdk_var}" = "Jdk-1.8" ]; then
      . ./include/Jdk1.8.sh
      Install-JDK18
  fi
  . ./include/Tomcat8.sh
  Install_Tomcat8 && array[12]=Tomcat-${tomcat8_version}
fi

# php server
if [ "${Php_var}" = "Php-5.3" ]; then
  . ./include/php5.3.sh
  Install_PHP53 && array[13]=PHP-${php53_version}
elif [ "${Php_var}" = "Php-5.4" ]; then
  . ./include/php5.4.sh
  Install_PHP54 && array[14]=PHP-${php54_version}
elif [ "${Php_var}" = "Php-5.5" ]; then
  . ./include/php5.5.sh
  Install_PHP55 && array[15]=PHP-${php55_version}
elif [ "${Php_var}" = "Php-5.6" ]; then
  . ./include/php5.6.sh
  Install_PHP56 && array[16]=PHP-${php56_version}
elif [ "${Php_var}" = 'Php-7.0' ]; then
  . ./include/php7.0.sh
  Install_PHP70 && array[17]=PHP-${php70_version}
elif [ "${Php_var}" = "Php-7.1" ]; then
  . ./include/php7.1.sh
  Install_PHP71 && array[18]=PHP-${php71_version}
fi

# ImageMagick or GraphicsMagick
if [ "${Magick_var}" == "ImageMagick" ]; then
  . include/ImageMagick.sh
  [ ! -d "/usr/local/imagemagick" ] && Install_ImageMagick
  [ ! -e "`$php_install_dir/bin/php-config --extension-dir`/imagick.so" ] && Install_php-imagick
elif [ "${Magick_var}" == "GraphicsMagick" ]; then
  . include/GraphicsMagick.sh
  [ ! -d "/usr/local/graphicsmagick" ] && Install_GraphicsMagick
  [ ! -e "`$php_install_dir/bin/php-config --extension-dir`/gmagick.so" ] && Install_php-gmagick
fi

# php cache
if [ "${Php_cache}" = "Zend OPcache" ]; then
  . ./include/ZendOpcache.sh
  Install_ZendOPcache
elif [ "${Php_cache}" = "XCache" ]; then
  . ./include/XCache.sh
  Install_XCache
elif [ "${Php_cache}" = "APCU" ]; then
  . ./include/APCU.sh
  Install_APCU
fi

# ftp
if [ "${Ftp_yn}" = 'y' ]; then
  . ./include/PureFtpd.sh
  Install_PureFTPd && array[19]=PureFtpd-${pureftpd_version}
fi

# redis
if [ "${Redis_yn}" = 'y' ]; then
  . ./include/redis.sh
  Install_redis-server && array[20]=Redis-${redis_version}
  if [ "${Php_yn}" = 'y' ]; then
    Install_php-redis
  fi
fi

# memcached
if [ "${Memcached_yn}" = 'y' ]; then
  . ./include/memcached.sh
  Install_memcached && array[21]=memcached-${memcached_version}
  if [ "${Mem_var}" = "php-extension-memcache" ]; then
    Install_php-memcache
  elif [ "${Mem_var}" = "php-extension-memcached" ]; then
    Install_php-memcached
  fi
fi

# ssdb
if [ "${Ssdb_yn}" = 'y' ]; then
  . ./include/ssdb.sh
  Install_Ssdb && array[22]=SSDB-${Ssdb_Version}
fi

# varnish
if [ "${Varnish_yn}" = 'y' ]; then
  . ./include/Varnish.sh
  Install_Varnish && array[23]=varnish-${varnish_version}
fi

# gitlab-ce
if [ "${Gitlab_yn}" = 'y' ]; then
  . ./include/gitlab-ce.sh
  Install_Gitlab-ce && array[23]=gitlab-ce
fi

# jenkins
if [ "${Jenkins_yn}" = 'y' ]; then
  . ./include/jenkins.sh
  Install_Jenkins
fi




# install complete
num=1

if [[ ${#array[@]} != 0 ]]; then
  echo -e "${CMESSAGE}This list is a software installation is complete: ${CEND}"
  for a in ${array[@]}; do
    echo -e "${CSUCCESSFUL}\t$num  ${a} ${CEND}"
    ((num++))
  done
fi

if [ -n "${dbrootpwd}" ]; then
  echo -e "${CSUCCESSFUL}database password: ${dbrootpwd}${CEND}"
fi

