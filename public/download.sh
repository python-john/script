#!/bin/bash

Download() {
  pushd ${Pwd}/src

  printf """
	====================================
  ======== Download Software =========
  ====================================
  """

  # download
  if [ ! -e "/usr/local/lib/libjemalloc.so" ]; then
	  if [ "$Countries" = "CN" ]; then
	    src_url=${OSS_Url}/jemalloc/jemalloc-${jemalloc_version}.tar.bz2 && Download_Http
	  else
	    src_url=https://github.com/jemalloc/jemalloc/releases/download/${jemalloc_version}/jemalloc-${jemalloc_version}.tar.bz2 && Download_Https
	  fi
	fi

	if [ ! -e "$(which tmux)" ]; then
	  if [ "$Countries" = 'CN' ]; then
		  src_url=${OSS_Url}/tmux/libevent-${libevent_version}.tar.gz && Download_Http
		  src_url=${OSS_Url}/tmux/tmux-${tmux_version}.tar.gz && Download_Http
		else
	 	  src_url=https://github.com/libevent/libevent/releases/download/release-${libevent_version}/libevent-${libevent_version}.tar.gz && Download_Http
	  	src_url=https://github.com/tmux/tmux/releases/download/${tmux_version}/tmux-${tmux_version}.tar.gz && Download_Https
	  fi
	fi

  if [ ! -e "$(which htop)" ]; then
  	if [ "$Countries" = 'CN' ]; then
		  src_url=${OSS_Url}/htop/htop-${htop_version}.tar.gz && Download_Http
  	else
		  src_url=http://hisham.hm/htop/releases/${htop_version}/htop-${htop_version}.tar.gz && Download_Http
  	fi
  fi



	# database
	if [ "${Mysql_var}" = "Mysql-5.5" ]; then
		if [ "$install_mod"  == 'binary' ]; then
		  src_url=https://mirrors.tuna.tsinghua.edu.cn/mysql/downloads/MySQL-5.5/mysql-${mysql55_version}-linux2.6-${SYS_BIT_b}.tar.gz &&  Download_Https
		elif [  "$install_mod" == 'source' ]; then
		  src_url=https://mirrors.tuna.tsinghua.edu.cn/mysql/downloads/MySQL-5.5/mysql-${mysql55_version}.tar.gz && Download_Https
		  src_url=${OSS_Url}/mysql/mysql5.5/mysql-5.5-fix-arm-client_plugin.patch && Download_Http
		fi

	elif [ "${Mysql_var}" = "Mysql-5.6" ]; then
		if [ "$install_mod"  == 'binary' ]; then
		  src_url=https://mirrors.tuna.tsinghua.edu.cn/mysql/downloads/MySQL-5.6/mysql-${mysql56_version}-linux-glibc2.12-${SYS_BIT_b}.tar.gz &&  Download_Https
		  src_url=http://mirrors.ustc.edu.cn/mysql-ftp/Downloads/MySQL-5.6/mysql-${mysql56_version}-linux-glibc2.12-${SYS_BIT_b}.tar.gz &&  Download_Https
		elif [  "$install_mod" == 'source' ]; then
		  src_url=https://mirrors.tuna.tsinghua.edu.cn/mysql/downloads/MySQL-5.6/mysql-${mysql56_version}.tar.gz && Download_Https
		  src_url=http://mirrors.ustc.edu.cn/mysql-ftp/Downloads/MySQL-5.6/mysql-${mysql56_version}.tar.gz && Download_Https
		fi

	elif [ "${Mysql_var}" = "Mysql-5.7" ]; then
  	boostVersion2=$(echo ${boost_version} | awk -F. '{print $1}')_$(echo ${boost_version} | awk -F. '{print $2}')_$(echo ${boost_version} | awk -F. '{print $3}')
    if [ "$Countries" = "CN" ]; then
      src_url=${OSS_Url}/boost/boost_${boostVersion2}.tar.gz && Download_Http
    else
  	  src_url=http://downloads.sourceforge.net/project/boost/boost/${boost_version}/boost_${boostVersion2}.tar.gz && Download_Http
    fi
    if [ "$install_mod"  == 'binary' ]; then
      src_url=https://mirrors.tuna.tsinghua.edu.cn/mysql/downloads/MySQL-5.7/mysql-${mysql57_version}-linux-glibc2.5-${SYS_BIT_b}.tar.gz &&  Download_Https
    elif [  "$install_mod" == 'source' ]; then
      src_url=https://mirrors.tuna.tsinghua.edu.cn/mysql/downloads/MySQL-5.7/mysql-${mysql57_version}.tar.gz && Download_Https
    fi

	elif [ "${Mysql_var}" = "Mariadb-10.0" ]; then
		if [ "${install_mod}" == "binary" ]; then
			# download
		  if [ "$Countries" = "CN" ]; then
			  src_url=https://mirrors.tuna.tsinghua.edu.cn/mariadb/mariadb-${mariadb100_version}/bintar-${GLIBC_FLAG}-${SYS_BIT_a}/mariadb-${mariadb100_version}-${GLIBC_FLAG}-${SYS_BIT_b}.tar.gz && Download_Https
			else
		    src_url=https://downloads.mariadb.org/interstitial/mariadb-${mariadb100_version}/bintar-${GLIBC_FLAG}-${SYS_BIT_a}/mariadb-${mariadb100_version}-${GLIBC_FLAG}-${SYS_BIT_b}.tar.gz && Download_Https
		  fi
		elif [ "${install_mod}" == "source" ]; then
			# download
		  if [ "$Countries" = "CN" ]; then
			  src_url=https://mirrors.tuna.tsinghua.edu.cn/mariadb/mariadb-${mariadb100_version}/source/mariadb-${mariadb100_version}.tar.gz && Download_Https
			else
		    src_url=https://downloads.mariadb.org/interstitial/mariadb-${mariadb100_version}/source/mariadb-${mariadb100_version}.tar.gz && Download_Https
		  fi
		fi

	elif [ "${Mysql_var}" = "Mariadb-10.1" ]; then
		if [ "${install_mod}" == "binary" ]; then
		  if [ "$Countries" = "CN" ]; then
		    src_url=https://mirrors.tuna.tsinghua.edu.cn/mariadb/mariadb-${mariadb101_version}/bintar-${GLIBC_FLAG}-${SYS_BIT_a}/mariadb-${mariadb101_version}-${GLIBC_FLAG}-${SYS_BIT_b}.tar.gz && Download_Https
		  else
		    src_url=https://downloads.mariadb.org/interstitial/mariadb-${mariadb101_version}/bintar-${GLIBC_FLAG}-${SYS_BIT_a}/mariadb-${mariadb101_version}-${GLIBC_FLAG}-${SYS_BIT_b}.tar.gz && Download_Https
		  fi
	  elif [ "${install_mod}" == "source" ]; then
	    if [ "$Countries" = "CN" ]; then
	      src_url=https://mirrors.tuna.tsinghua.edu.cn/mariadb/mariadb-${mariadb101_version}/source/mariadb-${mariadb101_version}.tar.gz && Download_Https
	    else
	      src_url=https://downloads.mariadb.org/interstitial/mariadb-${mariadb101_version}/source/mariadb-${mariadb101_version}.tar.gz && Download_Https
	    fi
	  fi

	elif [ "${Mysql_var}" = "Mariadb-5.5" ]; then
		if [ "${install_mod}" == "binary" ]; then
		  if [ "$Countries" = "CN" ]; then
		    src_url=https://mirrors.tuna.tsinghua.edu.cn/mariadb/mariadb-${mariadb55_version}/bintar-${GLIBC_FLAG}-${SYS_BIT_a}/mariadb-${mariadb55_version}-${GLIBC_FLAG}-${SYS_BIT_b}.tar.gz && Download_Https
		  else
		    src_url=https://downloads.mariadb.org/interstitial/mariadb-${mariadb55_version}/bintar-${GLIBC_FLAG}-${SYS_BIT_a}/mariadb-${mariadb55_version}-${GLIBC_FLAG}-${SYS_BIT_b}.tar.gz && Download_Https
		  fi
		elif [ "${install_mod}" == "source" ]; then
		  if [ "$Countries" = "CN" ]; then
		    src_url=https://mirrors.tuna.tsinghua.edu.cn/mariadb/mariadb-${mariadb55_version}/source/mariadb-${mariadb55_version}.tar.gz && Download_Https
		  else
		    src_url=https://downloads.mariadb.org/interstitial/mariadb-${mariadb55_version}/source/mariadb-${mariadb55_version}.tar.gz && Download_Https
		  fi
		fi

	fi

	# web server
	if [ "${Web_var}" = "Install Nginx" ]; then
    if [ "${Countries}" = "CN" ]; then
      src_url=${OSS_Url}/nginx/nginx-${nginx_version}.tar.gz && Download_Http
      src_url=${OSS_Url}/pcre/pcre-${pcre_version}.tar.gz && Download_Http
      src_url=${OSS_Url}/openssl/openssl-${openssl_version}.tar.gz && Download_Http
      src_url=${OSS_Url}/zlib/zlib-${zlib_version}.tar.gz && Download_Http
    else
		  src_url=http://nginx.org/download/nginx-${nginx_version}.tar.gz && Download_Http
		  src_url=https://ftp.pcre.org/pub/pcre/pcre-${pcre_version}.tar.gz && Download_Https
		  src_url=https://www.openssl.org/source/openssl-${openssl_version}.tar.gz && Download_Https
		  src_url=http://www.zlib.net/fossils/zlib-${zlib_version}.tar.gz && Download_Http
    fi
	elif [ "${Web_var}" = "Install Tengine" ]; then
    if [ "${Countries}" = "CN" ]; then
      src_url=${OSS_Url}/pcre/pcre-${pcre_version}.tar.gz && Download_Http
      src_url=${OSS_Url}/openssl/openssl-${openssl_version}.tar.gz && Download_Http
      src_url=${OSS_Url}/tengine/tengine-${tengine_version}.tar.gz && Download_Http
    else
		  src_url=https://ftp.pcre.org/pub/pcre/pcre-${pcre_version}.tar.gz && Download_Https
		  src_url=https://www.openssl.org/source/openssl-${openssl_version}.tar.gz && Download_Https
		  src_url=http://tengine.taobao.org/download/tengine-${tengine_version}.tar.gz && Download_Http
    fi

	elif [ "${Apache_var}" = "Apache-2.2" ]; then
		src_url=https://mirrors.aliyun.com/apache/httpd/httpd-${apache22_version}.tar.gz && Download_Https

	elif [ "${Apache_var}" = "Apache-2.4" ]; then
		if [ "$Countries" = "CN" ]; then
		  src_url=${OSS_Url}/pcre/pcre-${pcre_version}.tar.gz && Download_Http
		  src_url=https://mirrors.tuna.tsinghua.edu.cn/apache/apr/apr-${apr_version}.tar.gz && Download_Https
		  src_url=https://mirrors.tuna.tsinghua.edu.cn/apache/apr/apr-util-${apr_util_version}.tar.gz && Download_Https
      src_url=${OSS_Url}/nghttp2/nghttp2-${nghttp2_version}.tar.gz && Download_Http
		else
		  src_url=https://ftp.pcre.org/pub/pcre/pcre-${pcre_version}.tar.gz && Download_Https
		  src_url=http://www-eu.apache.org/dist/apr/apr-${apr_version}.tar.gz && Download_Http
		  src_url=http://www-eu.apache.org/dist/apr/apr-util-${apr_util_version}.tar.gz && Download_Http
		  src_url=https://github.com/nghttp2/nghttp2/releases/download/v${nghttp2_version}/nghttp2-${nghttp2_version}.tar.gz && Download_Https
		fi
	  src_url=https://mirrors.aliyun.com/apache/httpd/httpd-${apache24_version}.tar.gz && Download_Https

	elif [ "${Tomcat_var}" = "Tomcat-7" ]; then
		# download
		src_url=https://mirrors.aliyun.com/apache/apr/apr-${apr_version}.tar.gz && Download_Https
		src_url=http://mirror.bit.edu.cn/apache/tomcat/tomcat-7/v${tomcat7_version}/bin/apache-tomcat-${tomcat7_version}.tar.gz && Download_Http
		src_url=http://mirror.bit.edu.cn/apache/tomcat/tomcat-7/v${tomcat7_version}/bin/extras/catalina-jmx-remote.jar && Download_Http
		src_url=https://mirrors.aliyun.com/apache/tomcat/tomcat-connectors/native/1.2.12/source/tomcat-native-${native_version}-src.tar.gz && Download_Https
    if [ "${Countries}" = "CN" ]; then
      src_url=${OSS_Url}/openssl/openssl-${openssl_version}.tar.gz && Download_Http
    else
      src_url=https://www.openssl.org/source/openssl-${openssl_version}.tar.gz && Download_Https
    fi

	elif [ "${Tomcat_var}" = "Tomcat-8" ]; then
		src_url=https://mirrors.aliyun.com/apache/apr/apr-${apr_version}.tar.gz && Download_Https
		src_url=http://mirror.bit.edu.cn/apache/tomcat/tomcat-8/v${tomcat8_version}/bin/apache-tomcat-${tomcat8_version}.tar.gz && Download_Http
		src_url=http://mirror.bit.edu.cn/apache/tomcat/tomcat-8/v${tomcat8_version}/bin/extras/catalina-jmx-remote.jar && Download_Http
		src_url=https://mirrors.aliyun.com/apache/tomcat/tomcat-connectors/native/1.2.12/source/tomcat-native-${native_version}-src.tar.gz && Download_Https
    if [ "${Countries}" = "CN" ]; then
      src_url=${OSS_Url}/openssl/openssl-${openssl_version}.tar.gz && Download_Http
    else
      src_url=https://www.openssl.org/source/openssl-${openssl_version}.tar.gz && Download_Https
    fi

	fi

  # jdk
	if [ "${Jdk_var}" = "Jdk-1.7" ]; then
		JDK_FILE="jdk-`echo $jdk17_version | awk -F. '{print $2}'`u`echo $jdk17_version | awk -F_ '{print $NF}'`-linux-$SYS_BIG_FLAG.tar.gz"
		src_url=${OSS_Url}/jdk/${JDK_FILE} && Download_Http

	elif [ "${Jdk_var}" = "Jdk-1.8" ]; then
		JDK_FILE="jdk-`echo $jdk18_version | awk -F. '{print $2}'`u`echo $jdk18_version | awk -F_ '{print $NF}'`-linux-$SYS_BIG_FLAG.tar.gz"
		src_url=${OSS_Url}/jdk/${JDK_FILE} && Download_Http

	fi

	# php server
	if [ "${Php_var}" = "Php-5.3" ]; then
		if [ "$Countries" = "CN" ]; then
			src_url=${OSS_Url}/lib/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/curl/curl-${curl_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/lib/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mhash/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mcrypt/mcrypt-${mcrypt_version}.tar.gz && Download_Http
		else
			src_url=http://ftp.gnu.org/pub/gnu/libiconv/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=https://curl.haxx.se/download/curl-${curl_version}.tar.gz && Download_Https
			src_url=http://downloads.sourceforge.net/project/mcrypt/Libmcrypt/${libmcrypt_version}/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mhash/mhash/${mhash_version}/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mcrypt/MCrypt/${mcrypt_version}/mcrypt-${mcrypt_version}.tar.gz && Download_Http
		fi
		src_url=${OSS_Url}/php/libiconv-glibc-2.16.patch && Download_Http
		src_url=${OSS_Url}/php/fpm-race-condition.patch && Download_Http
		src_url=http://www.php.net/distributions/php-${php53_version}.tar.gz && Download_Http

	elif [ "${Php_var}" = "Php-5.4" ]; then
		if [ "$Countries" = "CN" ]; then
			src_url=${OSS_Url}/lib/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/curl/curl-${curl_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/lib/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mhash/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mcrypt/mcrypt-${mcrypt_version}.tar.gz && Download_Http
		else
			src_url=http://ftp.gnu.org/pub/gnu/libiconv/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=https://curl.haxx.se/download/curl-${curl_version}.tar.gz && Download_Https
			src_url=http://downloads.sourceforge.net/project/mcrypt/Libmcrypt/${libmcrypt_version}/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mhash/mhash/${mhash_version}/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mcrypt/MCrypt/${mcrypt_version}/mcrypt-${mcrypt_version}.tar.gz && Download_Http
	  fi
	  src_url=${OSS_Url}/php/libiconv-glibc-2.16.patch && Download_Http
	  src_url=${OSS_Url}/php/fpm-race-condition.patch && Download_Http
		src_url=http://www.php.net/distributions/php-${php54_version}.tar.gz && Download_Http

	elif [ "${Php_var}" = "Php-5.5" ]; then
		if [ "$Countries" = "CN" ]; then
			src_url=${OSS_Url}/lib/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/curl/curl-${curl_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/lib/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mhash/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mcrypt/mcrypt-${mcrypt_version}.tar.gz && Download_Http
		else
			src_url=http://ftp.gnu.org/pub/gnu/libiconv/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=https://curl.haxx.se/download/curl-${curl_version}.tar.gz && Download_Https
			src_url=http://downloads.sourceforge.net/project/mcrypt/Libmcrypt/${libmcrypt_version}/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mhash/mhash/${mhash_version}/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mcrypt/MCrypt/${mcrypt_version}/mcrypt-${mcrypt_version}.tar.gz && Download_Http
	  fi
	  src_url=${OSS_Url}/php/libiconv-glibc-2.16.patch && Download_Http
	  src_url=${OSS_Url}/php/fpm-race-condition.patch && Download_Http
		src_url=http://www.php.net/distributions/php-${php55_version}.tar.gz && Download_Http

	elif [ "${Php_var}" = "Php-5.6" ]; then
		if [ "$Countries" = "CN" ]; then
			src_url=${OSS_Url}/lib/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/curl/curl-${curl_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/lib/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mhash/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mcrypt/mcrypt-${mcrypt_version}.tar.gz && Download_Http
		else
			src_url=http://ftp.gnu.org/pub/gnu/libiconv/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=https://curl.haxx.se/download/curl-${curl_version}.tar.gz && Download_Https
			src_url=http://downloads.sourceforge.net/project/mcrypt/Libmcrypt/${libmcrypt_version}/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mhash/mhash/${mhash_version}/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mcrypt/MCrypt/${mcrypt_version}/mcrypt-${mcrypt_version}.tar.gz && Download_Http
	  fi
		src_url=${OSS_Url}/php/libiconv-glibc-2.16.patch && Download_Http
		src_url=http://www.php.net/distributions/php-${php56_version}.tar.gz && Download_Http

	elif [ "${Php_var}" = 'Php-7.0' ]; then
		if [ "$Countries" = "CN" ]; then
			src_url=${OSS_Url}/lib/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/curl/curl-${curl_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/lib/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mhash/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mcrypt/mcrypt-${mcrypt_version}.tar.gz && Download_Http
		else
			src_url=http://ftp.gnu.org/pub/gnu/libiconv/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=https://curl.haxx.se/download/curl-${curl_version}.tar.gz && Download_Https
			src_url=http://downloads.sourceforge.net/project/mcrypt/Libmcrypt/${libmcrypt_version}/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mhash/mhash/${mhash_version}/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mcrypt/MCrypt/${mcrypt_version}/mcrypt-${mcrypt_version}.tar.gz && Download_Http
		fi
		src_url=${OSS_Url}/lib/libiconv-glibc-2.16.patch && Download_Http
		src_url=http://www.php.net/distributions/php-${php70_version}.tar.gz && Download_Http

	elif [ "${Php_var}" = "Php-7.1" ]; then
		if [ "$Countries" = "CN" ]; then
			src_url=${OSS_Url}/lib/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/curl/curl-${curl_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/lib/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mhash/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=${OSS_Url}/mcrypt/mcrypt-${mcrypt_version}.tar.gz && Download_Http
		else
			src_url=http://ftp.gnu.org/pub/gnu/libiconv/libiconv-${libiconv_version}.tar.gz && Download_Http
			src_url=https://curl.haxx.se/download/curl-${curl_version}.tar.gz && Download_Https
			src_url=http://downloads.sourceforge.net/project/mcrypt/Libmcrypt/${libmcrypt_version}/libmcrypt-${libmcrypt_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mhash/mhash/${mhash_version}/mhash-${mhash_version}.tar.gz && Download_Http
			src_url=http://downloads.sourceforge.net/project/mcrypt/MCrypt/${mcrypt_version}/mcrypt-${mcrypt_version}.tar.gz && Download_Http
		fi
		src_url=h${OSS_Url}/php/libiconv-glibc-2.16.patch && Download_Http
		src_url=http://www.php.net/distributions/php-${php71_version}.tar.gz && Download_Http

	fi

	# ImageMagick or GraphicsMagick
	if [ "${Magick_var}" == "ImageMagick" ]; then
		# src_url=http://mirror.checkdomain.de/imagemagick/ImageMagick-${ImageMagick_version}.tar.gz && Download_Http
		src_url=${OSS_Url}/imagemagick/ImageMagick-${ImageMagick_version}.tar.gz && Download_Http
	elif [ "${Magick_var}" == "GraphicsMagick" ]; then
	  # src_url=http://downloads.sourceforge.net/project/graphicsmagick/GraphicsMagick/${GraphicsMagick_version}/GraphicsMagick-${GraphicsMagick_version}.tar.gz && Download_Http
		src_url=${OSS_Url}/graphicsmagick/GraphicsMagick-${GraphicsMagick_version}.tar.gz && Download_Http
	fi

	# ftp
	if [ "${Ftp_yn}" = 'y' ]; then
		if [ "$Countries" = "CN" ]; then
			src_url=${OSS_Url}/pureftpd/pure-ftpd-${pureftpd_version}.tar.gz && Download_Http
		else
			src_url=https://download.pureftpd.org/pub/pure-ftpd/releases/pure-ftpd-${pureftpd_version}.tar.gz && Download_Https
		fi
	fi

	# redis
	if [ "${Redis_yn}" = 'y' ]; then
		if [ "$Countries" = "CN" ]; then
			src_url=${OSS_Url}/redis/redis-${redis_version}.tar.gz && Download_Http
		else
			src_url=http://download.redis.io/releases/redis-${redis_version}.tar.gz && Download_Http
		fi
	fi

	# memcached
	if [ "${Memcached_yn}" = 'y' ]; then
		src_url=http://www.memcached.org/files/memcached-${memcached_version}.tar.gz &&  Download_Http
	fi

	# ssdb
	if [ "${Ssdb_yn}" = 'y' ]; then
		src_url=https://github.com/ideawu/ssdb/archive/${Ssdb_Version}.tar.gz && Download_Https
	fi

  # varnish
  if [ "${Varnish_yn}" = 'y' ]; then
  	src_url=http://repo.varnish-cache.org/source/varnish-${varnish_version}.tar.gz && Download_Http
  fi

  # zabbix
  if [ "${Zabbix_yn}" = 'y' ];then
  	src_url=http://sourceforge.net/projects/zabbix/files/ZABBIX%20Latest%20Stable/${zabbix_version}/zabbix-${zabbix_version}.tar.gz && Download_Http
  fi

  popd
}

