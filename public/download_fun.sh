#!/bin/bash

Download_Http() {
	wget -c --tries=6 $src_url || { echo -e "${CFAIL} Download ${src_url} error !!!${CEND}";kill -9 $$; }
}

Download_Https() {
	wget -c --tries=6 --no-check-certificate $src_url || { echo -e "${CFAIL} Download ${src_url} error !!!${CEND}";kill -9 $$; }
}