#!/usr/bin/env python
# coding:utf-8
import urllib,json

class Get_Countries(object):

		def Get_LocalIp(self):
				Get_LocalIP_Url = 'http://icanhazip.com/'
				try:
						data = urllib.urlopen(Get_LocalIP_Url)
						IP = data.read().strip('\n')
				except:
						IP = 'error'

				Get_PublicIP_Url = 'http://ip.taobao.com/service/getIpInfo.php?ip=%s' % IP
				try:
						data =  urllib.urlopen(Get_PublicIP_Url).read()
						PublicIP = json.loads(data)
						Countries = PublicIP['data']['country_id']
				except Exception,e :
						return Exception,e
				return Countries

if __name__ == "__main__":
		Countries_data = Get_Countries()
		print Countries_data.Get_LocalIp()