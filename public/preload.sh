#!/bin/bash


export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
clear
# loading public config
. ./public/color.sh
. ./public/sysinfo.sh
. ./public/options.conf
. ./public/versions.conf
. ./public/download.sh
. ./public/download_fun.sh
. ./public/auther.sh

# auther
Auther

# check run user
[ $(id -u) != "0" ] && { echo -e "{CFAIL}please use the root run script${CEND}"; exit 1; }

# check system
Get_Sysinfo
if [  "${OS}" = "Other" ]; then
  echo -e "${FAIL} Error,system dose not support${CEND}"
  kill -9 $$
fi

# check network
if [ "$(curl -o /dev/null -s -w "%{http_code}" www.baidu.com)" -ne '200' ]; then
  echo -e "${CFAIL} Error,Please check your network...${CEND}"
  kill -9 $$
fi

# create data directory
[ ! -d "${data_dir}" ] && mkdir -p ${data_dir}


# get ip countries 'CN'
Countries=$(python ./public/get_countries.py)
