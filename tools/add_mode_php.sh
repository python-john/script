#!/bin/bash

sed -i "s@^Pwd.*@Pwd=`pwd`@" ./public/options.conf
# Load public config
. ./public/preload.sh


sleep 1

# check run user
[ $(id -u) != "0" ] && { echo "{CFAIL}please use the root run script${CEND}"; exit 1; }

# check system
Get_Sysinfo
if [  "${OS}" = "Other" ]; then
	echo -e "${CFAIL} error,system dose not support${CEND}"
  kill -9 $$
fi

while :; do echo
  select Mode_var in "Apcu" "ImageMagick" "GraphicsMagick" "Memcache" "Memcached" "Redis" "Msgpack" "Ssh2"; do echo
    if [ "${Mode_var}" = "Apcu" ]; then
    	if [ ! "`${php_install_dir}/bin/php -m | grep apcu`" ]; then
    		. ./include/APCU.sh
    	  Install_APCU
    	else
    		echo -e "${CFAIL} php extension apcu already existing ${CEND}"
    	fi
    elif [ "${Mode_var}" = "ImageMagick" ]; then
    	if [ ! "`${php_install_dir}/bin/php -m | grep imagick`" ]; then
	    	. ./include/ImageMagick.sh
	    	if [ ! -f "/usr/local/imagemagick/bin/magick" ]; then
	    	  Install_ImageMagick
	      fi
	    	Install_php-imagick
	    else
	    	echo -e "${CFAIL} php extension Imagemagick already existing ${CEND}"
	    fi
    elif [ "${Mode_var}" = "GraphicsMagick" ]; then
    	if [ ! "`${php_install_dir}/bin/php -m | grep gmagick`" ]; then
    	  . ./include/GraphicsMagick.sh
    	  if [ ! -f "/usr/local/graphicsmagick/bin/gm" ]; then
    	  	Install_GraphicsMagick
    	  fi
    	  Install_php-gmagick
      else
      	echo -e "${CFAIL} php extension GraphicsMagick already existing ${CEND}"
      fi
    elif [ "${Mode_var}" = "Memcache" ]; then
    	. ./include/memcached.sh
    	if [ ! -f "${memcached_install_dir}/bin/memcached" ]; then
            select Memcache_var in "Y" "N"; do echo
                if [ "${Memcache_var}" = "Y" ]; then
    	       	   Install_memcached
                fi
            done
	    fi
    	Install_php-memcache
    elif [ "${Mode_var}" = "Memcached" ]; then
    	. ./include/memcached.sh
    	if [ ! -f "${memcached_install_dir}/bin/memcached" ]; then
            select "Memcached_var" in "Y" "N"; do echo
                if [ "${Memcached_var}" = "Y" ]; then
        	    	Install_memcached
                fi
            done
	    fi
    	Install_php-memcached
    elif [ "${Mode_var}" = "Redis" ]; then
    	. ./include/redis.sh
    	if [ ! -f "${redis_install_dir}/bin/redis-server" ]; then
            select Redis_var in "Y" "N"; do echo
              if [ "${Redis_var}" = "Y" ]; then
        		Install_redis-server
              fi
            done
    	fi
    	Install_php-redis
    elif [ "${Mode_var}" = "Msgpack" ]; then
        . ./include/msgpack.sh
        Install_Msgpack
    elif [ "${Mode_var}" == "Ssh2" ]; then
        . ./include/ssh2.sh
        Install_Ssh2
    fi
    break
  done
  break
done

# php-ext
b=(bcmath bz2 calendar com_dotnet ctype curl date dba dom enchant exif fileinfo filter ftp gd gettext gmp hash iconv imap interbase intl json ldap libxml mbstring mcrypt mysqli mysqlnd oci8 odbc opcache openssl pcntl pcre pdo pdo_dblib pdo_firebird pdo_mysql pdo_oci pdo_odbc pdo_pgsql pdo_sqlite pgsql phar posix pspell readline recode reflection session shmop simplexml skeleton snmp soap sockets spl sqlite3 standard sysvmsg sysvsem sysvshm tidy tokenizer wddx xml xmlreader xmlrpc xmlwriter xsl zip zlib)

while :; do echo
    read -p "Do you want to input php module install? [y/n]" Module_yn
    if [[ $Module_yn =~ ^[y,n]$ ]]; then
        if [[ $Module_yn =~ 'y' ]]; then
            read -p "Please input you want to install is module name: " module_name
            for i in ${b[@]}; do if [[ $module_name = $i ]]; then b=y;echo $b;fi ;done
            if [[ $b =~ ^[y]$ ]]; then
                if `${php_install_dir}/bin/php -m | grep ${module_name}`; then
                    echo -e "${CMESSAGE} php extension already install ${CEND}"
                else
                    . ./include/Install_php_module.sh && Install_Php_Module
                fi
                break
            else
                echo -e "${CFAIL}Module in the source code package was not found, please manually install ${CEND}"
                break
            fi
        else
            break
        fi
    else
        echo -e "${CMESSAGE}INPUT ERROR,only input 'y' or 'n'...${CEND}"
    fi
done
