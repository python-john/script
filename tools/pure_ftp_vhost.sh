#!/bin/bash

clear
. ./public/options.conf
. ./public/color.sh

[ $(id -u) != "0" ] && { echo -e  "${CFAILD}Error: You must be root to run this script${CEND}"; exit 1; }

[ ! -d "${pureftpd_install_dir}" ] && { echo -e "${CFAILD}FTP server does not exist! ${CEND}"; exit 1; }

FTP_conf=${pureftpd_install_dir}/etc/pure-ftpd.conf
FTP_tmp_passfile=${pureftpd_install_dir}/etc/pureftpd_psss.tmp
Puredbfile=${pureftpd_install_dir}/etc/pureftpd.pdb
Passwdfile=${pureftpd_install_dir}/etc/pureftpd.passwd
FTP_bin=${pureftpd_install_dir}/bin/pure-pw
[ -z "`grep ^PureDB $FTP_conf`" ] && { echo -e "${CFAILURE}pure-ftpd is not own password database${CEND}" ; exit 1; }

USER() {
while :; do echo
  read -p "Please input a username: " User
  if [ -z "$User" ]; then
    echo -e "${CFAILD}username can't be NULL! ${CEND}"
  else
    break
  fi
done
}

PASSWORD() {
while :; do echo
  read -p "Please input the password: " Password
  [ -n "`echo $Password | grep '[+|&]'`" ] && { echo -e "${CFAILD}input error,not contain a plus sign (+) and &${CEND}"; continue; }
  if (( ${#Password} >= 5 ));then
    echo -e "${Password}\n$Password" > $FTP_tmp_passfile
    break
  else
    echo "${CFAILD}Ftp password least 5 characters! ${CEND}"
  fi
done
}

DIRECTORY() {
while :; do echo
  read -p "Please input the directory(Default directory: ${wwwroot_dir}): " Directory
  if [ -z "$Directory" ]; then
    Directory="$wwwroot_dir"
  fi
  if [ ! -d "$Directory" ]; then
    echo -e "${CFAILD}The directory does not exist${CEND}"
  else
    break
  fi
done
}

while :; do
  printf "
What Are You Doing?
\t${CMESSAGE}1${CEND}. UserAdd
\t${CMESSAGE}2${CEND}. UserMod
\t${CMESSAGE}3${CEND}. UserPasswd
\t${CMESSAGE}4${CEND}. UserDel
\t${CMESSAGE}5${CEND}. ListAllUser
\t${CMESSAGE}6${CEND}. ShowUser
\t${CMESSAGE}q${CEND}. Exit
"
  read -p "Please input the correct option: " Number
  if [[ ! $Number =~ ^[1-6,q]$ ]]; then
    echo -e "${CFAILD}input error! Please only input 1 ~ 6 and q${CEND}"
  else
    case "$Number" in
    1)
      USER
      [ -e "$Passwdfile" ] && [ -n "`grep ^${User}: $Passwdfile`" ] && { echo -e "${CFAILD}[$User] is already existed! ${CEND}"; continue; }
      PASSWORD;DIRECTORY
      $FTP_bin useradd $User -f $Passwdfile -u $run_user -g $run_user -d $Directory -m < $FTP_tmp_passfile
      $FTP_bin mkdb $Puredbfile -f $Passwdfile > /dev/null 2>&1
      echo "#####################################"
      echo
      echo "[$User] create successful! "
      echo
      echo -e "You user name is : ${CMESSAGE}$User${CEND}"
      echo -e "You Password is : ${CMESSAGE}$Password${CEND}"
      echo -e "You directory is : ${CMESSAGE}$Directory${CEND}"
      echo
      ;;
    2)
      USER
      [ -e "$Passwdfile" ] && [ -z "`grep ^${User}: $Passwdfile`" ] && { echo -e "${CFAILD}[$User] was not existed! ${CEND}"; continue; }
      DIRECTORY
      $FTP_bin usermod $User -f $Passwdfile -d $Directory -m
      $FTP_bin mkdb $Puredbfile -f $Passwdfile > /dev/null 2>&1
      echo "#####################################"
      echo
      echo "[$User] modify a successful! "
      echo
      echo -e "You user name is : ${CMESSAGE}$User${CEND}"
      echo -e "You new directory is : ${CMESSAGE}$Directory${CEND}"
      echo
      ;;
    3)
      USER
      [ -e "$Passwdfile" ] && [ -z "`grep ^${User}: $Passwdfile`" ] && { echo -e "${CMESSAGE}[$User] was not existed! ${CEND}"; continue; }
      PASSWORD
      $FTP_bin passwd $User -f $Passwdfile -m < $FTP_tmp_passfile
      $FTP_bin mkdb $Puredbfile -f $Passwdfile > /dev/null 2>&1
      echo "#####################################"
      echo
      echo "[$User] Password changed successfully! "
      echo
      echo -e "You user name is : ${CMESSAGE}$User${CEND}"
      echo -e "You new password is : ${CMESSAGE}$Password${CEND}"
      echo
      ;;
    4)
      if [ ! -e "$Passwdfile" ]; then
        echo -e "${CFAILD}User was not existed! ${CEND}"
      else
        $FTP_bin list
      fi

      USER
      [ -e "$Passwdfile" ] && [ -z "`grep ^${User}: $Passwdfile`" ] && { echo -e "${CFAILD}[$User] was not existed! ${CEND}"; continue; }
      $FTP_bin userdel $User -f $Passwdfile -m
      $FTP_bin mkdb $Puredbfile -f $Passwdfile > /dev/null 2>&1
      echo
      echo "[$User] have been deleted! "
      ;;
    5)
      if [ ! -e "$Passwdfile" ]; then
        echo -e "${CFAILD}User was not existed! ${CEND}"
      else
        $FTP_bin list
      fi
      ;;
    6)
      USER
      [ -e "$Passwdfile" ] && [ -z "`grep ^${User}: $Passwdfile`" ] && { echo -e "${CFAILD}[$User] was not existed! ${CEND}"; continue; }
      $FTP_bin show $User
      ;;
    q)
      exit
      ;;
    esac
  fi
done
