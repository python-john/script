#!/bin/bash

# Load public config
. ./public/preload.sh

Upgrade_PHP() {
  pushd ${Pwd}/src > /dev/null
  [ ! -e "${php_install_dir}" ] && echo -e "${CFAIL}PHP is not installed on your system! ${CEND}" && exit 1
  echo
  OLD_PHP_version=`${php_install_dir}/bin/php -r 'echo PHP_VERSION;'`
  echo -e "Current PHP Version: ${CMESSAGE}$OLD_PHP_version${CEND}"
  while :; do echo
    read -p "Please input upgrade PHP Version: " NEW_PHP_version
    if [ "${NEW_PHP_version%.*}" == "${OLD_PHP_version%.*}" ]; then
      [ ! -e "php-${NEW_PHP_version}.tar.gz" ] && src_url=http://www.php.net/distributions/php-${NEW_PHP_version}.tar.gz && Download_Http
      if [ -e "php-${NEW_PHP_version}.tar.gz" ]; then
        echo -e "Download [${CMESSAGE}php-${NEW_PHP_version}.tar.gz${CEND}] successfully! "
      else
        echo -e "${CFAIL}PHP version does not exist! ${CEND}"
      fi
      break
    else
      echo -e "${CFAIL}input error! ${CEND}Please only input '${CMESSAGE}${OLD_PHP_version%.*}.xx${CEND}'"
    fi
  done

  if [ -e "php-${NEW_PHP_version}.tar.gz" ]; then
    echo -e "[${CMESSAGE}php-${NEW_PHP_version}.tar.gz${CEND}] found"
    echo  "Press Ctrl+c to cancel or Press any key to continue..."
    char=`get_char`
    tar xzf php-${NEW_PHP_version}.tar.gz
    cp ${Pwd}/src/fpm-race-condition.patch ./
    patch -d php-${NEW_PHP_version} -p0 < fpm-race-condition.patch
    pushd php-${NEW_PHP_version}
    make clean
    ${php_install_dir}/bin/php -i |grep 'Configure Command' | awk -F'=>' '{print $2}' | bash
    make ZEND_EXTRA_LIBS='-liconv'
    echo -e "${CMESSAGE}Stoping php-fpm...${CEND}"
    service php-fpm stop
    make install
    echo "${CMESSAGE}Starting php-fpm...${CEND}"
    service php-fpm start
    popd
    echo -e "You have ${CSUCCESSFUL}successfully${CEND} upgrade from ${CMESSAGE}$OLD_PHP_version${CEND} to ${CMESSAGE}${NEW_PHP_version}${CEND}"
    rm -rf php-${NEW_PHP_version}
  fi
  popd > /dev/null
}
Upgrade_PHP
